<?php
namespace Modules\Account\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
class InputBuilder 
{
    /**
     * Builder data into request and config
     * 
     * @param Request $request
     * @param string $apiName
     * 
     * @return array
     */
    public static function buildInputIntoRequestConfig(Request $request, string $apiName)
    {
        foreach(config('account.route.'. $apiName .'.request') as $table => $value) {
            $tableConfig = config('account.entities.'. $table .'.column');
            $fillable = config('account.entities.'. $table .'.fillable');
            $data[$table] = [];
            foreach ($value['column'] as $key => $columnName) {
                if (!array_key_exists($columnName, $tableConfig)) {
                    continue;
                }

                if (!in_array($columnName, $fillable)) {
                    Log::warning(__FILE__ . " : " . __LINE__ . " Config >>" . $columnName . " not in array account.entities.'. $table .'.fillable");
                    continue;
                }

                if ($request->has($columnName)) {
                    $data[$table][$columnName] = $request->get($columnName);
                }
            }

            $defaultColumn = array_diff_key($tableConfig, $data[$table]);
            foreach ($defaultColumn as $columnName => $setting) {
                if (!in_array($columnName, $fillable)) {
                    Log::warning(__FILE__ . " : " . __LINE__ . " Config >>" . $columnName . " not in array account.entities.'. $table .'.fillable");
                    continue;
                }

                if ($tableConfig[$columnName]['default'] === false 
                    && !array_key_exists('foreign_key', $tableConfig[$columnName])
                    && $tableConfig[$columnName]['foreign_key'] === false
                ) {
                    Log::warning(__FILE__ . " : " . __LINE__ . " Config >>" . $columnName . " not values .'. $table .'.fillable");
                    continue;
                }

                $data[$table][$columnName] = $setting['default'];
            }
        }

        return $data;
    }

    /**
     * Builder data into request and config
     * 
     * @param Request $request
     * @param string $apiName
     * 
     * @return array
     */
    public static function buildValidateIntoRequestConfig(Request $request, string $apiName)
    {
        foreach(config('account.route.'. $apiName .'.request') as $table => $value) {
            $tableConfig = config('account.entities.'. $table .'.column');
            $fillable = config('account.entities.'. $table .'.fillable');
            $rule[$table] = [];
            foreach ($value['column'] as $key => $columnName) {
                if (!array_key_exists($columnName, $tableConfig)) {
                    continue;
                }

                if (!in_array($columnName, $fillable)) {
                    Log::warning(__FILE__ . " : " . __LINE__ . " Config >>" . $columnName . " not in array account.entities.'. $table .'.fillable");
                    continue;
                }

                foreach ($tableConfig[$columnName] as $key => $value) {
                    if ($key == "max") {
                        $rule[$table][$columnName][] = $key.':'.$value;
                    }

                    if ($key == "nullable" && $value === false) {
                        $rule[$table][$columnName][] = 'required';
                    }

                    if ($key == "type") {
                        $rule[$table][$columnName][] = $value;
                    }
                }
            }
        }

        return $rule;
    }
}
