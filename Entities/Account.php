<?php

namespace Modules\Account\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Schema;

class Account extends Model
{
    use HasFactory;

    public function __construct(array $attributes = [])
    {
        $this->table = config('account.entities.account.table');
        $this->primaryKey = config('account.entities.account.primary_key');
        $this->fillable = config('account.entities.account.fillable');
        $this->casts = config('account.entities.profile.casts');
        $this->hidden = config('account.entities.account.hidden');
        parent::__construct($attributes);
    }

    protected static function newFactory()
    {
        return \Modules\Account\Database\factories\AccountFactory::new();
    }

    public function profile()
    {
        return $this->hasOne(
            Profile::class,
            config('account.entities.account.relationship.profile.foreign_key'),
            config('account.entities.account.relationship.profile.owner_key')
        )->latest();
    }
}
