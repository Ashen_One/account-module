<?php

namespace Modules\Account\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profile extends Model
{
    use HasFactory;

    public function __construct(array $attributes = [])
    {
        $this->table = config('account.entities.profile.table');
        $this->primaryKey = config('account.entities.profile.primary_key');
        $this->fillable = config('account.entities.profile.fillable');
        $this->casts = config('account.entities.profile.casts');
        parent::__construct($attributes);
    }

    protected static function newFactory()
    {
        return \Modules\Account\Database\factories\ProfileFactory::new();
    }

    public function account()
    {
        return $this->belongsTo(
            Account::class,
            config('account.entities.profile.relationship.account.foreign_key'),
            config('account.entities.profile.relationship.account.owner_key')
        )->latest();
    }
}
