<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTtProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tt_profiles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('first_name', 50)->nullable()->default("Họ");
            $table->string('last_name', 50)->nullable()->default("Tên");
            $table->string('phone', 50)->nullable()->default("0123456789");
            $table->date('brith_day')->nullable();
            $table->string('address', 255)->nullable()->default("Đia chỉ của bạn");
            $table->string('zipcode', 255)->nullable()->default("0000000");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tt_profiles');
    }
}
