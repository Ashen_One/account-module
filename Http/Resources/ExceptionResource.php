<?php

namespace Modules\Account\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class ExceptionResource extends JsonResource
{
    protected $messages;
    protected $apiKey;

    public function __construct($apiKey, array $messages = [])
    {
        $this->messages = $messages;
        $this->apiKey = $apiKey;
        parent::__construct([]);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'code' => config($this->apiKey) ?: config('api.code.unknown_error'),
            'detail' => __($this->apiKey),
            'errors' => $this->when(!empty($this->messages), $this->messages),
        ];
    }
}