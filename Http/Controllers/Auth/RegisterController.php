<?php

namespace Modules\Account\Http\Controllers\Auth;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Modules\Account\Helpers\InputBuilder;
use Modules\Account\Http\Requests;
use Modules\Account\Entities as Models;
use Modules\Account\Http\Resources\Auth as Resources;

class RegisterController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function register(Requests\RegisterRequest $request)
    {
        $data = InputBuilder::buildInputIntoRequestConfig($request, 'register');
        $accountData = $data['account'];
        $accountData['password'] = bcrypt($data['account']['password']);
        $profileData = $data['profile'];

        DB::beginTransaction();
        try {
            $account = Models\Account::create($accountData);

            $profileData['user_id'] = $account->id;
            $profile =  Models\Profile::create($profileData);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error(__FILE__ . " : " . __LINE__ . " Catch >>" . $th);
            throw $th;
        }

        $account->load(['profile']);

        return new Resources\RegisterResource($account);
    }
}