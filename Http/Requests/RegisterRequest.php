<?php

namespace Modules\Account\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Modules\Account\Entities\Account;
use Modules\Account\Entities\Profile;
use Modules\Account\Helpers\InputBuilder;

class RegisterRequest extends BaseApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $requestRules = InputBuilder::buildValidateIntoRequestConfig(request(), 'register');
        return $requestRules;
    }

    /**
     * Get the validation messages error to the request.
     *
     * @return array
     */

    public function messages()
    {
        return [
            //
        ];
    }
}
