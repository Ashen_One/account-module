<?php

namespace Modules\Account\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class BaseApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
    }

    /**
     * Get the validation messages error to the request.
     *
     * @return array
     */

    public function messages()
    {
        
    }


    /**
     * Overide function failedValidation() in Class Formrequest
     * 
     * @param Validator $validator
     * @return HttpResponseException
     * 
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(
            [
                'status' => JsonResponse::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $errors,
                'results' => '',
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );

    }
}
