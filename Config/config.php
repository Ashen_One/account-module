<?php

return [
    'name' => 'Account',
    'entities' => [
        'account' => [
            'name' => 'Account',
            'table' => 'tt_accounts',
            'primary_key' => 'id',
            'fillable' => [
                'name',
                'email',
                'password',
                'status',
                'email_verified_at',
                'remember_token',
            ],
            'column' => [
                'name' => [
                    'type' => 'string',
                    'max' => 255,
                    'nullable' => false,
                    'default' => false,
                    'unique' => true,
                ],
                'email' => [
                    'type' => 'email',
                    'max' => 255,
                    'nullable' => false,
                    'default' => false,
                    'unique' => true,
                ],
                'email_verified_at' => [
                    'type' => 'date_time',
                    'nullable' => true,
                    'default' => null,
                ],
                'password' => [
                    'type' => 'password',
                    'max' => 255,
                    'nullable' => false,
                    'default' => false,
                ],
                'status' => [
                    'type' => 'integer',
                    'nullable' => false,
                    'default' => 0,
                ],
                'remember_token' => [
                    'type' => 'string',
                    'max' => 100,
                    'nullable' => true,
                    'default' => null,
                ],
            ],
            'hidden' => [
                'password',
                'remember_token',
            ],
            'casts' => [
                'email_verified_at' => 'datetime',
            ],
            'relationship' => [
                'profile' => [
                    'entity' => 'profile',
                    'foreign_key' => 'user_id',
                    'owner_key' => 'id',
                ]
            ]
        ],
        'profile' => [
            'name' => 'Profile',
            'table' => 'tt_profiles',
            'primary_key' => 'id',
            'fillable' => [
                'first_name', 
                'last_name', 
                'phone', 
                'brith_day', 
                'address',
                'user_id',
                'zipcode',
            ],
            'column' => [
                'user_id' => [
                    'type' => 'integer',
                    'nullable' => false,
                    'default' => false,
                    'foreign_key' => true,
                ],
                'first_name' => [
                    'type' => 'string',
                    'max' => 50,
                    'nullable' => true,
                    'default' => "Họ",
                ],
                'last_name' => [
                    'type' => 'string',
                    'max' => 50,
                    'nullable' => true,
                    'default' => "Tên",
                ],
                'phone' => [
                    'type' => 'string',
                    'max' => 50,
                    'nullable' => true,
                    'default' => "0123456789",
                ],
                'brith_day' => [
                    'type' => 'date',
                    'nullable' => true,
                    'default' => null,
                ],
                'address' => [
                    'type' => 'string',
                    'max' => 255,
                    'nullable' => true,
                    'default' => "Địa chỉ của bạn",
                ],
                'zipcode' => [
                    'type' => 'string',
                    'max' => 255,
                    'nullable' => true,
                    'default' => "0000000",
                ],
            ],
            'casts' => [
                'brith_day' => 'string',
            ],
            'relationship' => [
                'account' => [
                    'entity' => 'account',
                    'foreign_key' => 'user_id',
                    'owner_key' => 'id',
                ]
            ],
        ],
    ],
    'route' => [
        'register' => [
            'name' => 'api.register',
            'url' => 'register',
            'request' => [
                'account' => [
                    'column' => [
                        'name',
                        'email',
                        'password',
                    ],
                ],
                'profile' => [
                    'column' => [
                        'first_name',
                        'last_name',
                        'phone',
                        'brith_day',
                        'address',
                        'zipcode',
                    ],
                ],
            ],
        ],
    ],
];
