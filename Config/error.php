<?php

return [
    'code' => [
        'unknown_error' => '4000',
        'unauthenticated' => '4001',
        'password_not_correct' => '4002',
        'permission_denied' => '4003',
        'not_found' => '4004',
        'token_not_correct' => '4005',
        'invalid_parameters' => '4022',
        'system_error' => '5000',
    ],
];